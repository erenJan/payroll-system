package login;
import menu.admin_menu;
import menu.asisstant_menu;
import menu.user_menu;

import sun.audio.*;
import java.sql.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.io.*;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import java.awt.Button;
import java.awt.SystemColor;
import javax.swing.JTextField;
import javax.swing.JSeparator;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.io.InputStream;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class login extends JFrame {

	private JPanel contentPane;
	private JTextField username;
	
	
	int xx,xy;
	private JPasswordField password;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					login frame = new login();
					frame.setUndecorated(true);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	
	// going to borrow code from a gist to move frame.
	

	/**
	 * Create the frame.
	 */
	
	
	public login() {
		
		
		
		setBackground(Color.WHITE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 729, 476);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(0, 0, 51));
		panel.setBounds(0, 1, 365, 489);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel label = new JLabel("");
		
		
		label.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				
				 xx = e.getX();
			     xy = e.getY();
			}
		});
		label.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent arg0) {
				
				int x = arg0.getXOnScreen();
	            int y = arg0.getYOnScreen();
	            login.this.setLocation(x - xx, y - xy);  
			}
		});
		label.setText("Payroll S");
		label.setBounds(-80, 0, 483, 275);
		label.setVerticalAlignment(SwingConstants.TOP);
		label.setIcon(new ImageIcon("D:\\Developer\\Developing Workspace\\Eclipse\\nesenesel\\src\\OFAF.gif"));
		panel.add(label);
		
		JLabel lblAdvancedTech = new JLabel("Advanced Tech");
		lblAdvancedTech.setHorizontalAlignment(SwingConstants.CENTER);
		lblAdvancedTech.setForeground(new Color(240, 248, 255));
		lblAdvancedTech.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblAdvancedTech.setBounds(72, 341, 184, 54);
		panel.add(lblAdvancedTech);
		
		JLabel lblCompanyPayrollSystem = new JLabel("Company Payroll System");
		lblCompanyPayrollSystem.setHorizontalAlignment(SwingConstants.CENTER);
		lblCompanyPayrollSystem.setForeground(new Color(240, 248, 255));
		lblCompanyPayrollSystem.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblCompanyPayrollSystem.setBounds(10, 306, 336, 54);
		panel.add(lblCompanyPayrollSystem);
		
		
		
		
		
		
		Button button = new Button("Sign In");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					Class.forName("com.mysql.jdbc.Driver");
					Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/ceng_323","root","");
					Statement stmt=con.createStatement();
					String sql="Select * from auth where username='"+username.getText()+"' and password='"+password.getText().toString()+"'";
					ResultSet rs=stmt.executeQuery(sql);
					if(rs.next()) {
						String stp=rs.getString("status");
						if(stp.equals("admin")) {
							admin_menu admin = new admin_menu();
							admin.main(null);
							
						}
						if(stp.equals("assistant")) {
							asisstant_menu assistant = new asisstant_menu();
							assistant.main(null);
							
						}
						if(stp.equals("user")) {
							user_menu user = new user_menu();
							user.main(null);
						}
					dispose();	
					con.close();
					}
					
					else
						JOptionPane.showMessageDialog(null, "Login Failed...");
				}catch(Exception e){
					System.out.print(e);
				}
			}
		});
		button.setForeground(Color.WHITE);
		button.setBackground(new Color(241, 57, 83));
		button.setBounds(395, 287, 283, 36);
		contentPane.add(button);
		
		username = new JTextField();
		username.setBounds(395, 131, 283, 36);
		contentPane.add(username);
		username.setColumns(10);
		
		JLabel lblUsername = new JLabel("USERNAME");
		lblUsername.setBounds(395, 106, 114, 14);
		contentPane.add(lblUsername);
		
		
		
		
		
		JLabel lbl_close = new JLabel("X");
		lbl_close.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				System.exit(0);
			}
		});
		lbl_close.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_close.setForeground(new Color(241, 57, 83));
		lbl_close.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lbl_close.setBounds(691, 0, 37, 27);
		contentPane.add(lbl_close);
		
		JLabel lblPassword = new JLabel("PASSWORD");
		lblPassword.setBounds(395, 178, 114, 14);
		contentPane.add(lblPassword);
		
		Button button_1 = new Button("Sign Up");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				register register_page = new register();
				register_page.main(null);
				dispose();
				
			}
		});
		button_1.setForeground(Color.WHITE);
		button_1.setBackground(new Color(241, 57, 83));
		button_1.setBounds(395, 342, 283, 36);
		contentPane.add(button_1);
		
		password = new JPasswordField();
		password.setBounds(395, 212, 283, 36);
		contentPane.add(password);
	}
}
