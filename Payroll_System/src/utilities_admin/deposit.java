package utilities_admin;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import login.login;
import menu.user_menu;
import utilities_user.prepare_objection;



public class deposit extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					deposit frame = new deposit();
					frame.setUndecorated(true);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public deposit() {
		setBackground(Color.WHITE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 729, 476);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		JLabel label_1 = new JLabel("X");
		label_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				System.exit(0);
			}
		});
		label_1.setHorizontalAlignment(SwingConstants.CENTER);
		label_1.setForeground(new Color(241, 57, 83));
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 18));
		label_1.setBounds(686, 5, 37, 27);
		contentPane.add(label_1);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(5, 13, 31));
		panel.setBounds(0, 0, 346, 490);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblAssistantPanel = new JLabel("Deposit Page");
		lblAssistantPanel.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblAssistantPanel.setForeground(Color.RED);
		lblAssistantPanel.setBounds(10, 50, 188, 31);
		panel.add(lblAssistantPanel);
		
		JLabel label_2 = new JLabel("Employee Payroll System");
		label_2.setForeground(Color.WHITE);
		label_2.setFont(new Font("Tahoma", Font.PLAIN, 20));
		label_2.setBounds(10, 11, 336, 46);
		panel.add(label_2);
		
		Button button_1 = new Button("Payroll");
		button_1.setForeground(Color.WHITE);
		button_1.setBackground(Color.RED);
		button_1.setBounds(27, 236, 283, 36);
		panel.add(button_1);
		
		Button button_2 = new Button("Previous Payrolls\r\n");
		button_2.setForeground(Color.WHITE);
		button_2.setBackground(Color.RED);
		button_2.setBounds(27, 289, 283, 36);
		panel.add(button_2);
		
		Button button_3 = new Button("Objections");
		button_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				prepare_objection objection = new prepare_objection();
				objection.main(null);
				dispose();
			}
		});
		button_3.setForeground(Color.WHITE);
		button_3.setBackground(Color.RED);
		button_3.setBounds(27, 342, 283, 36);
		panel.add(button_3);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(345, -70, 483, 611);
		contentPane.add(panel_1);
		
		JLabel label = new JLabel("Payroll S");
		panel_1.add(label);
		label.setForeground(Color.WHITE);
		label.setFont(new Font("Tahoma", Font.PLAIN, 20));
		label.setVerticalAlignment(SwingConstants.TOP);
		label.setIcon(new ImageIcon("D:\\Developer\\Developing Workspace\\Eclipse\\nesenesel\\src\\back.gif"));
		JLabel lblEmployeePayrollSystem = new JLabel("Employee Payroll System");
		lblEmployeePayrollSystem.setForeground(Color.WHITE);
		lblEmployeePayrollSystem.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblEmployeePayrollSystem.setBounds(10, 11, 281, 38);
	}

}
