package login;

import java.sql.*;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import java.awt.Button;
import java.awt.SystemColor;
import javax.swing.JTextField;
import javax.swing.JSeparator;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class register extends JFrame {

	private JPanel contentPane;
	private JTextField username;

	Connection con = null;
	PreparedStatement pst = null;

	int xx, xy;
	private JPasswordField password;
	private JPasswordField passwordagain;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					register frame = new register();
					frame.setUndecorated(true);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public register() {
		setBackground(Color.WHITE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 729, 476);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBackground(new Color(0, 0, 51));
		panel.setBounds(0, 0, 365, 490);
		contentPane.add(panel);
		panel.setLayout(null);
		JLabel label = new JLabel();
		label.setText("Payroll S");
		label.setBounds(-80, 0, 483, 275);
		label.setVerticalAlignment(SwingConstants.TOP);
		label.setIcon(new ImageIcon("D:\\Developer\\Developing Workspace\\Eclipse\\nesenesel\\src\\OFAF.gif"));
		panel.add(label);

		JLabel label_1 = new JLabel("Company Payroll System");
		label_1.setHorizontalAlignment(SwingConstants.CENTER);
		label_1.setForeground(new Color(240, 248, 255));
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 30));
		label_1.setBounds(5, 300, 336, 54);
		panel.add(label_1);
		

		JLabel label_2 = new JLabel("Advanced Tech");
		label_2.setHorizontalAlignment(SwingConstants.CENTER);
		label_2.setForeground(new Color(240, 248, 255));
		label_2.setFont(new Font("Tahoma", Font.PLAIN, 20));
		label_2.setBounds(82, 335, 184, 54);
		panel.add(label_2);

		Button button = new Button("SignUp");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					if (!password.getText().equals(passwordagain.getText())) {
						JOptionPane.showMessageDialog(null, "Register Failed! passwords must equal!");
						return ;
					}

					if (username.getText().isEmpty() || password.getText().isEmpty()) {
						JOptionPane.showMessageDialog(null, "Register Failed! Fill both field!");
						return;

					}

					String query = "INSERT INTO `auth`(`username`, `password`) VALUES (?,?)";
					con = DriverManager.getConnection("jdbc:mysql://localhost:3306/ceng_323", "root", "");
					pst = con.prepareStatement(query);
					pst.setString(1, username.getText());
					pst.setString(2, password.getText().toString());
					pst.executeUpdate();
					JOptionPane.showMessageDialog(null, "Register Succesfully");
					login login_page = new login();
					login_page.main(null);
					dispose();
				} catch (Exception e) {

				}

			}
		});
		button.setForeground(Color.WHITE);
		button.setBackground(new Color(241, 57, 83));
		button.setBounds(395, 301, 283, 36);
		contentPane.add(button);

		JLabel lblUsername = new JLabel("USERNAME");
		lblUsername.setBounds(395, 79, 114, 14);
		contentPane.add(lblUsername);

		username = new JTextField();
		username.setColumns(10);
		username.setBounds(395, 104, 283, 36);
		contentPane.add(username);

		JLabel lblPassword = new JLabel("PASSWORD");
		lblPassword.setBounds(395, 153, 96, 14);
		contentPane.add(lblPassword);

		JLabel lbl_close = new JLabel("X");
		lbl_close.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {

				System.exit(0);
			}
		});
		lbl_close.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_close.setForeground(new Color(241, 57, 83));
		lbl_close.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lbl_close.setBounds(691, 0, 37, 27);
		contentPane.add(lbl_close);

		JLabel lblRepassword = new JLabel("RE-PASSWORD\r\n");
		lblRepassword.setBounds(395, 225, 96, 14);
		contentPane.add(lblRepassword);

		password = new JPasswordField();
		password.setBounds(395, 178, 283, 36);
		contentPane.add(password);

		passwordagain = new JPasswordField();
		passwordagain.setBounds(395, 250, 283, 36);
		contentPane.add(passwordagain);
		
		Button button_1 = new Button("< Sign In Page");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				login back = new login();
				back.main(null);
				dispose();
			}
		});
		button_1.setForeground(Color.WHITE);
		button_1.setBackground(new Color(241, 57, 83));
		button_1.setBounds(395, 389, 96, 27);
		contentPane.add(button_1);
	}
}
