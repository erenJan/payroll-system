package utilities_admin.add_delete;

import java.sql.*;
import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import utilities_admin.add_delete.add_delete_page;
import login.login;
import menu.admin_menu;
import javax.swing.JTextField;

public class search_employee extends JFrame {

	private JPanel contentPane;
	private JTextField search_text;
	String s;
	
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					search_employee frame = new search_employee();
					frame.setUndecorated(true);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public class Function {
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement ps = null;

		public ResultSet find(String s) {
			try {
				con = DriverManager.getConnection("jdbc:mysql://localhost:3306/ceng_323", "root", "");
				ps = con.prepareStatement("select * from employees where username = ?");
				ps.setString(1, s);
				rs = ps.executeQuery();
			} catch (Exception ex) {
				JOptionPane.showMessageDialog(null, ex.getMessage());
			}
			return rs;
		}

	}

	public search_employee() {
		setBackground(Color.WHITE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 729, 476);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel label_1 = new JLabel("X");
		label_1.addMouseListener(new MouseAdapter() {// close window
			@Override
			public void mouseClicked(MouseEvent arg0) {
				System.exit(0);
			}
		});
		label_1.setHorizontalAlignment(SwingConstants.CENTER);
		label_1.setForeground(new Color(241, 57, 83));
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 18));
		label_1.setBounds(686, 5, 37, 27);
		contentPane.add(label_1);

		JPanel panel = new JPanel();
		panel.setBackground(new Color(5, 13, 31));
		panel.setBounds(0, 0, 346, 490);
		contentPane.add(panel);
		panel.setLayout(null);

		JLabel lblAssistantPanel = new JLabel("Search Employee");
		lblAssistantPanel.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblAssistantPanel.setForeground(Color.RED);
		lblAssistantPanel.setBounds(10, 50, 212, 31);
		panel.add(lblAssistantPanel);

		JLabel label_2 = new JLabel("Employee Payroll System");
		label_2.setForeground(Color.WHITE);
		label_2.setFont(new Font("Tahoma", Font.PLAIN, 20));
		label_2.setBounds(10, 11, 336, 46);
		panel.add(label_2);

		Button button_3 = new Button("Previous Page\r\n");
		button_3.addActionListener(new ActionListener() {// go back main menu
			public void actionPerformed(ActionEvent arg0) {
				add_delete_page back = new add_delete_page();
				back.main(null);
				dispose();

			}
		});
		button_3.setForeground(Color.WHITE);
		button_3.setBackground(Color.RED);
		button_3.setBounds(10, 390, 126, 36);
		panel.add(button_3);
		JLabel label_3 = new JLabel("");
		label_3.setForeground(Color.WHITE);
		label_3.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_3.setBounds(10, 154, 86, 20);
		panel.add(label_3);

		JLabel label_4 = new JLabel("");
		label_4.setForeground(Color.WHITE);
		label_4.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_4.setBounds(10, 185, 86, 20);
		panel.add(label_4);

		JLabel label_5 = new JLabel("");
		label_5.setForeground(Color.WHITE);
		label_5.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_5.setBounds(10, 216, 86, 20);
		panel.add(label_5);

		JLabel label_6 = new JLabel("");
		label_6.setForeground(Color.WHITE);
		label_6.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_6.setBounds(10, 247, 86, 20);
		panel.add(label_6);

		JLabel label_7 = new JLabel("");
		label_7.setForeground(Color.WHITE);
		label_7.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_7.setBounds(10, 278, 86, 20);
		panel.add(label_7);

		JLabel label_8 = new JLabel("");
		label_8.setForeground(Color.WHITE);
		label_8.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_8.setBounds(10, 311, 86, 20);
		panel.add(label_8);
		JLabel lblSalary = new JLabel("");
		lblSalary.setForeground(Color.WHITE);
		lblSalary.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblSalary.setBounds(10, 344, 86, 20);
		panel.add(lblSalary);
		
		JLabel label_16 = new JLabel("");
		label_16.setForeground(Color.WHITE);
		label_16.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_16.setBounds(106, 342, 12, 20);
		panel.add(label_16);

		JLabel label_9 = new JLabel("");
		label_9.setForeground(Color.WHITE);
		label_9.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_9.setBounds(106, 154, 12, 20);
		panel.add(label_9);

		JLabel label_10 = new JLabel("");
		label_10.setForeground(Color.WHITE);
		label_10.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_10.setBounds(106, 185, 12, 20);
		panel.add(label_10);

		JLabel label_11 = new JLabel("");
		label_11.setForeground(Color.WHITE);
		label_11.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_11.setBounds(106, 216, 12, 20);
		panel.add(label_11);

		JLabel label_12 = new JLabel("");
		label_12.setForeground(Color.WHITE);
		label_12.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_12.setBounds(106, 247, 12, 20);
		panel.add(label_12);

		JLabel label_13 = new JLabel("");
		label_13.setForeground(Color.WHITE);
		label_13.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_13.setBounds(106, 278, 12, 20);
		panel.add(label_13);

		JLabel label_14 = new JLabel("");
		label_14.setForeground(Color.WHITE);
		label_14.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_14.setBounds(106, 309, 12, 20);
		panel.add(label_14);

		JLabel username = new JLabel("");
		username.setForeground(Color.WHITE);
		username.setFont(new Font("Tahoma", Font.PLAIN, 14));
		username.setBounds(136, 154, 86, 20);
		panel.add(username);

		JLabel surname_display = new JLabel("");
		surname_display.setForeground(Color.WHITE);
		surname_display.setFont(new Font("Tahoma", Font.PLAIN, 14));
		surname_display.setBounds(136, 185, 86, 20);
		panel.add(surname_display);

		JLabel gender_display = new JLabel("");
		gender_display.setForeground(Color.WHITE);
		gender_display.setFont(new Font("Tahoma", Font.PLAIN, 14));
		gender_display.setBounds(136, 216, 86, 20);
		panel.add(gender_display);

		JLabel department_display = new JLabel("");
		department_display.setForeground(Color.WHITE);
		department_display.setFont(new Font("Tahoma", Font.PLAIN, 14));
		department_display.setBounds(136, 247, 86, 20);
		panel.add(department_display);

		JLabel work_display = new JLabel("");
		work_display.setForeground(Color.WHITE);
		work_display.setFont(new Font("Tahoma", Font.PLAIN, 14));
		work_display.setBounds(136, 278, 86, 20);
		panel.add(work_display);

		JLabel birth_display = new JLabel("");
		birth_display.setForeground(Color.WHITE);
		birth_display.setFont(new Font("Tahoma", Font.PLAIN, 14));
		birth_display.setBounds(136, 311, 86, 20);
		panel.add(birth_display);

		JLabel salary_display = new JLabel("");
		salary_display.setForeground(Color.WHITE);
		salary_display.setFont(new Font("Tahoma", Font.PLAIN, 14));
		salary_display.setBounds(136, 342, 86, 20);
		panel.add(salary_display);
		
		JLabel lblUserId = new JLabel("");
		lblUserId.setForeground(Color.WHITE);
		lblUserId.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblUserId.setBounds(236, 406, 55, 20);
		panel.add(lblUserId);
		
		JLabel user_id_display = new JLabel("");
		user_id_display.setForeground(Color.WHITE);
		user_id_display.setFont(new Font("Tahoma", Font.PLAIN, 14));
		user_id_display.setBounds(291, 406, 45, 20);
		panel.add(user_id_display);
		
		JLabel image_label = new JLabel("");
		image_label.setBounds(216, 50, 120, 124);
		panel.add(image_label);

		

		search_text = new JTextField();
		search_text.setToolTipText("Username");
		search_text.setColumns(10);
		search_text.setBounds(10, 104, 126, 20);
		panel.add(search_text);

		Button button = new Button("Search");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Function f = new Function();
				ResultSet rs = null;
				String fn = "fname";

				rs = f.find(search_text.getText());
				try {
					if (rs.next()) {
						label_3.setText("Name");
						label_4.setText("Surname");
						label_5.setText("Gender");
						label_6.setText("Department");
						label_7.setText("Work Plan");
						label_8.setText("Date of Birth");
						lblSalary.setText("Salary");
						lblUserId.setText("user id :");
						label_16.setText(":");
						label_9.setText(":");
						label_10.setText(":");
						label_11.setText(":");
						label_12.setText(":");
						label_13.setText(":");
						label_14.setText(":");
						
						username.setText(rs.getString("username"));
						surname_display.setText(rs.getString("usersurname"));
						gender_display.setText(rs.getString("gender"));
						department_display.setText(rs.getString("department"));
						work_display.setText(rs.getString("workplan"));
						birth_display.setText(rs.getString("dateofbirth"));
						salary_display.setText(rs.getString("salary"));
						user_id_display.setText(rs.getString("id"));
						byte[] img = rs.getBytes("image");
		                ImageIcon imageIcon = new ImageIcon(new ImageIcon(img).getImage().getScaledInstance(image_label.getWidth(), image_label.getHeight(), Image.SCALE_SMOOTH));
		                image_label.setIcon(imageIcon);
						
						
					} else {
						JOptionPane.showMessageDialog(null, "NO DATA FOR THIS ID");
					}
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(null, ex.getMessage());
				}
			}

		});
		button.setForeground(Color.WHITE);
		button.setBackground(Color.RED);
		button.setBounds(147, 104, 63, 20);
		panel.add(button);
		
		
		
		
		
		

		JPanel panel_1 = new JPanel();
		panel_1.setBounds(345, -70, 483, 611);
		contentPane.add(panel_1);
		
		JLabel label = new JLabel("Payroll S");
		panel_1.add(label);
		label.setForeground(Color.WHITE);
		label.setFont(new Font("Tahoma", Font.PLAIN, 20));
		label.setVerticalAlignment(SwingConstants.TOP);
		label.setIcon(new ImageIcon("D:\\Developer\\Developing Workspace\\Eclipse\\nesenesel\\src\\back.gif"));
		JLabel lblEmployeePayrollSystem = new JLabel("Employee Payroll System");
		lblEmployeePayrollSystem.setForeground(Color.WHITE);
		lblEmployeePayrollSystem.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblEmployeePayrollSystem.setBounds(10, 11, 281, 38);
	}

	public ImageIcon ResizeImage(String imgPath) {
		ImageIcon MyImage = new ImageIcon(imgPath);
		Image img = MyImage.getImage();
		//Image newImage = img.getScaledInstance(image_label.getWidth(), image_label.getHeight(), Image.SCALE_SMOOTH);
		ImageIcon image = new ImageIcon(img);
		return image;
	}
}
