package utilities_assistant;
import menu.asisstant_menu;
import java.sql.*;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import login.login;
import java.awt.Button;
import javax.swing.JEditorPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;

public class prepare_objection extends JFrame {

	private JPanel contentPane;
	private JTextField txtEmail;
	PreparedStatement pst = null;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					prepare_objection frame = new prepare_objection();
					frame.setUndecorated(true);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public prepare_objection() {
		setBackground(Color.WHITE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 729, 476);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		JLabel label_1 = new JLabel("X");
		label_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				System.exit(0);
			}
		});
		label_1.setHorizontalAlignment(SwingConstants.CENTER);
		label_1.setForeground(new Color(241, 57, 83));
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 18));
		label_1.setBounds(686, 5, 37, 27);
		contentPane.add(label_1);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(5, 13, 31));
		panel.setBounds(0, 0, 346, 490);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblAssistantPanel = new JLabel("Prepare Objection\r\n");
		lblAssistantPanel.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblAssistantPanel.setForeground(Color.RED);
		lblAssistantPanel.setBounds(10, 50, 212, 31);
		panel.add(lblAssistantPanel);
		
		JLabel label_2 = new JLabel("Employee Payroll System");
		label_2.setForeground(Color.WHITE);
		label_2.setFont(new Font("Tahoma", Font.PLAIN, 20));
		label_2.setBounds(10, 11, 336, 46);
		panel.add(label_2);
		
		Button button_3 = new Button("Previous Page\r\n");
		button_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {//go back main menu 
				asisstant_menu back = new asisstant_menu();
				back.main(null);
				dispose();
				
			}
		});
		button_3.setForeground(Color.WHITE);
		button_3.setBackground(Color.RED);
		button_3.setBounds(10, 390, 126, 36);
		panel.add(button_3);
		
		JEditorPane objection = new JEditorPane();
		objection.setToolTipText("Objection Message");
		objection.setBounds(10, 134, 326, 240);
		panel.add(objection);
		
		Button button = new Button("Send \r\n");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {//send objection 
				try {
					String str="";
					if(!(txtEmail.getText().equals(str))){
						String query=("INSERT INTO `objection`(`email`, `objection`) VALUES (?,?)");
						Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/ceng_323","root","");
						pst=con.prepareStatement(query);
						pst.setString(1, txtEmail.getText());
						pst.setString(2, objection.getText());
						
						pst.executeUpdate();
						pst.close();
						JOptionPane.showMessageDialog(null, "Sent Successfully");
						prepare_objection reload = new prepare_objection();
						reload.main(null);
						dispose();
						
						
						
					}else {
						JOptionPane.showMessageDialog(null, "Email Required!");
					}
					
					
				}catch(Exception e) {
					
				}
			}
		});
		button.setForeground(Color.WHITE);
		button.setBackground(Color.RED);
		button.setBounds(224, 390, 112, 36);
		panel.add(button);
		
		txtEmail = new JTextField();
		txtEmail.setToolTipText("E-mail\r\n");
		txtEmail.setBounds(10, 103, 192, 20);
		panel.add(txtEmail);
		txtEmail.setColumns(10);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(345, -70, 483, 611);
		contentPane.add(panel_1);

		JLabel label = new JLabel("Payroll S");
		panel_1.add(label);
		label.setForeground(Color.WHITE);
		label.setFont(new Font("Tahoma", Font.PLAIN, 20));
		label.setVerticalAlignment(SwingConstants.TOP);
		label.setIcon(new ImageIcon("D:\\Developer\\Developing Workspace\\Eclipse\\nesenesel\\src\\back.gif"));
		JLabel lblEmployeePayrollSystem = new JLabel("Employee Payroll System");
		lblEmployeePayrollSystem.setForeground(Color.WHITE);
		lblEmployeePayrollSystem.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblEmployeePayrollSystem.setBounds(10, 11, 281, 38);
	}
}

