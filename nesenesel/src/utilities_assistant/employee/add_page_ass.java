package utilities_assistant.employee;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.*;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;



import login.login;
import menu.admin_menu;
import java.awt.Choice;
import javax.swing.JRadioButton;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

public class add_page_ass extends JFrame {

	private JPanel contentPane;
	private JTextField userName;
	private JTextField surname;
	String s;
	JLabel image_label;
	private JTextField birth;
	private JTextField gender;
	private JTextField department;
	private JTextField workplan;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					add_page_ass frame = new add_page_ass();
					frame.setUndecorated(true);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public add_page_ass() {
		
		setBackground(Color.WHITE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 729, 476);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel label_1 = new JLabel("X");
		label_1.addMouseListener(new MouseAdapter() {// close window
			@Override
			public void mouseClicked(MouseEvent arg0) {
				System.exit(0);
			}
		});
		label_1.setHorizontalAlignment(SwingConstants.CENTER);
		label_1.setForeground(new Color(241, 57, 83));
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 18));
		label_1.setBounds(686, 5, 37, 27);
		contentPane.add(label_1);

		JPanel panel = new JPanel();
		panel.setBackground(new Color(5, 13, 31));
		panel.setBounds(0, 0, 346, 490);
		contentPane.add(panel);
		panel.setLayout(null);

		JLabel lblAssistantPanel = new JLabel("Add Employee Page");
		lblAssistantPanel.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblAssistantPanel.setForeground(Color.RED);
		lblAssistantPanel.setBounds(10, 50, 212, 31);
		panel.add(lblAssistantPanel);

		JLabel label_2 = new JLabel("Employee Payroll System");
		label_2.setForeground(Color.WHITE);
		label_2.setFont(new Font("Tahoma", Font.PLAIN, 20));
		label_2.setBounds(10, 11, 336, 46);
		panel.add(label_2);

		Button button_3 = new Button("Previous Page\r\n");
		button_3.addActionListener(new ActionListener() {// go back main menu
			public void actionPerformed(ActionEvent arg0) {
				employee back = new employee();
				back.main(null);
				dispose();

			}
		});
		button_3.setForeground(Color.WHITE);
		button_3.setBackground(Color.RED);
		button_3.setBounds(10, 390, 126, 36);
		panel.add(button_3);

		userName = new JTextField();
		userName.setBounds(136, 149, 126, 20);
		panel.add(userName);
		userName.setColumns(10);

		surname = new JTextField();
		surname.setColumns(10);
		surname.setBounds(136, 180, 126, 20);
		panel.add(surname);

		JLabel lblName = new JLabel("Name ");
		lblName.setForeground(Color.WHITE);
		lblName.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblName.setBounds(10, 149, 86, 20);
		panel.add(lblName);

		JLabel lblSurname = new JLabel("Surname\r\n");
		lblSurname.setForeground(Color.WHITE);
		lblSurname.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblSurname.setBounds(10, 180, 86, 20);
		panel.add(lblSurname);

		JLabel lblDepartment = new JLabel("Gender");
		lblDepartment.setForeground(Color.WHITE);
		lblDepartment.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblDepartment.setBounds(10, 211, 86, 20);
		panel.add(lblDepartment);

		JLabel lblDepartment_1 = new JLabel("Department\r\n");
		lblDepartment_1.setForeground(Color.WHITE);
		lblDepartment_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblDepartment_1.setBounds(10, 242, 86, 20);
		panel.add(lblDepartment_1);
		
		

		JLabel lblWorkPlan = new JLabel("Work Plan");
		lblWorkPlan.setForeground(Color.WHITE);
		lblWorkPlan.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblWorkPlan.setBounds(10, 273, 86, 20);
		panel.add(lblWorkPlan);
		

		JLabel lblPhotoOfEmployee = new JLabel("Employee's Photo\r\n\r\n");
		lblPhotoOfEmployee.setForeground(Color.WHITE);
		lblPhotoOfEmployee.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblPhotoOfEmployee.setBounds(10, 337, 126, 20);
		panel.add(lblPhotoOfEmployee);

		JButton btnNewButton = new JButton("Browse\r\n");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser fileChooser = new JFileChooser();
				fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
				FileNameExtensionFilter filter = new FileNameExtensionFilter(".IMAGE", "jpg", "gif", "png", "jpeg");
				fileChooser.addChoosableFileFilter(filter);
				int result = fileChooser.showSaveDialog(null);
				if (result == JFileChooser.APPROVE_OPTION) {
					File selectedFile = fileChooser.getSelectedFile();
					String path = selectedFile.getAbsolutePath();
					//image_label.setIcon(ResizeImage(path));
					s = path;
				} else if (result == JFileChooser.CANCEL_OPTION) {
					System.out.println("No Data");
				}
			}
		});
		
		
		btnNewButton.setBounds(136, 337, 126, 20);
		panel.add(btnNewButton);
		
		Button button = new Button("Add");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					String query = ("INSERT INTO `employees`(`username`, `usersurname`, `gender`, `department`, `workplan`, `dateofbirth`, `image`) VALUES (?,?,?,?,?,?,?)");
					Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/ceng_323","root","");
					PreparedStatement pst=con.prepareStatement(query);
					InputStream is = new FileInputStream(new File(s));
					pst.setString(1, userName.getText());
					pst.setString(2, surname.getText());
					
					pst.setString(3, gender.getText());
					pst.setString(4, department.getText());
					pst.setString(5, workplan.getText());
					pst.setString(6, birth.getText());
					pst.setBlob(7, is);
					pst.executeUpdate();
					con.close();
					JOptionPane.showMessageDialog(null, "Employee Added to Queue \nAdmin Will Add Employee");
					add_page_ass reload = new add_page_ass();
					reload.main(null);
					dispose();
					
				}catch(Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		
		
		
		button.setForeground(Color.WHITE);
		button.setBackground(Color.RED);
		button.setBounds(199, 390, 126, 36);
		panel.add(button);
		
		JLabel lblDateOfBirth = new JLabel("Date of Birth");
		lblDateOfBirth.setForeground(Color.WHITE);
		lblDateOfBirth.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblDateOfBirth.setBounds(10, 306, 86, 20);
		panel.add(lblDateOfBirth);
		
		JLabel lblDdmmyy = new JLabel("(DD/MM/YY)");
		lblDdmmyy.setForeground(Color.WHITE);
		lblDdmmyy.setFont(new Font("Tahoma", Font.PLAIN, 10));
		lblDdmmyy.setBounds(268, 306, 86, 20);
		panel.add(lblDdmmyy);
		
		birth = new JTextField();
		birth.setColumns(10);
		birth.setBounds(136, 306, 126, 20);
		panel.add(birth);
		
		gender = new JTextField();
		gender.setColumns(10);
		gender.setBounds(136, 213, 126, 20);
		panel.add(gender);
		
		department = new JTextField();
		department.setColumns(10);
		department.setBounds(136, 244, 126, 20);
		panel.add(department);
		
		workplan = new JTextField();
		workplan.setColumns(10);
		workplan.setBounds(136, 275, 126, 20);
		panel.add(workplan);
		
		
		
		

		JPanel panel_1 = new JPanel();
		panel_1.setBounds(345, -70, 483, 611);
		contentPane.add(panel_1);
		
		JLabel label = new JLabel("Payroll S");
		panel_1.add(label);
		label.setForeground(Color.WHITE);
		label.setFont(new Font("Tahoma", Font.PLAIN, 20));
		label.setVerticalAlignment(SwingConstants.TOP);
		label.setIcon(new ImageIcon("D:\\Developer\\Developing Workspace\\Eclipse\\nesenesel\\src\\back.gif"));
		JLabel lblEmployeePayrollSystem = new JLabel("Employee Payroll System");
		lblEmployeePayrollSystem.setForeground(Color.WHITE);
		lblEmployeePayrollSystem.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblEmployeePayrollSystem.setBounds(10, 11, 281, 38);
	}
	
	public ImageIcon ResizeImage(String imgPath){
        ImageIcon MyImage = new ImageIcon(imgPath);
        Image img = MyImage.getImage();
        Image newImage = img.getScaledInstance(image_label.getWidth(), image_label.getHeight(),Image.SCALE_SMOOTH);
        ImageIcon image = new ImageIcon(img);
        return image;
    }
}
