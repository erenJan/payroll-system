package utilities_user;
import menu.user_menu;
import utilities_admin.add_delete.search_employee.Function;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import login.login;
import menu.user_menu;
import javax.swing.JTextField;

public class current_payroll extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JLabel salary_message;
	private JLabel salary_display;
	private JLabel month_display;
	private JLabel id_req;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					current_payroll frame = new current_payroll();
					frame.setUndecorated(true);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public class Function {
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement ps = null;

		public ResultSet find(String s) {
			try {
				con = DriverManager.getConnection("jdbc:mysql://localhost:3306/ceng_323", "root", "");
				ps = con.prepareStatement("select * from employees where id = ?");
				ps.setString(1, s);
				rs = ps.executeQuery();
			} catch (Exception ex) {
				JOptionPane.showMessageDialog(null, ex.getMessage());
			}
			return rs;
		}

	}
	
	public current_payroll() {
		setBackground(Color.WHITE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 729, 476);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		JLabel label_1 = new JLabel("X");
		label_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				System.exit(0);
			}
		});
		label_1.setHorizontalAlignment(SwingConstants.CENTER);
		label_1.setForeground(new Color(241, 57, 83));
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 18));
		label_1.setBounds(686, 5, 37, 27);
		contentPane.add(label_1);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(5, 13, 31));
		panel.setBounds(0, 0, 346, 490);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblAssistantPanel = new JLabel("Current Payroll");
		lblAssistantPanel.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblAssistantPanel.setForeground(Color.RED);
		lblAssistantPanel.setBounds(10, 50, 188, 31);
		panel.add(lblAssistantPanel);
		
		JLabel label_2 = new JLabel("Employee Payroll System");
		label_2.setForeground(Color.WHITE);
		label_2.setFont(new Font("Tahoma", Font.PLAIN, 20));
		label_2.setBounds(10, 11, 336, 46);
		panel.add(label_2);
		
		Button button = new Button("Previous Page\r\n");
		button.addActionListener(new ActionListener() {//go back user menu 
			public void actionPerformed(ActionEvent arg0) {
				user_menu back = new user_menu();
				back.main(null);
				dispose();
			}
		});
		button.setForeground(Color.WHITE);
		button.setBackground(Color.RED);
		button.setBounds(10, 390, 126, 36);
		panel.add(button);
		
		
		
		textField = new JTextField();
		textField.setToolTipText("Username");
		textField.setColumns(10);
		textField.setBounds(10, 90, 126, 20);
		panel.add(textField);
		
		JLabel am;
		id_req = new JLabel("Employee ID");
		id_req.setFont(new Font("Times New Roman", Font.PLAIN, 20));
		id_req.setForeground(Color.WHITE);
		id_req.setBounds(216, 82, 126, 31);
		panel.add(id_req);
		
		
		JLabel lblNewLabel = new JLabel("Current Month :");
		lblNewLabel.setFont(new Font("Times New Roman", Font.PLAIN, 20));
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setBounds(32, 208, 126, 31);
		panel.add(lblNewLabel);
		lblNewLabel.setVisible(false);
	
		salary_message = new JLabel("Current Salary :");
		salary_message.setFont(new Font("Times New Roman", Font.PLAIN, 20));
		salary_message.setForeground(Color.WHITE);
		salary_message.setBounds(32, 239, 126, 31);
		panel.add(salary_message);
		salary_message.setVisible(false);
		
		JLabel salary;
		month_display = new JLabel("");
		month_display.setFont(new Font("Times New Roman", Font.PLAIN, 20));
		month_display.setForeground(Color.WHITE);
		month_display.setBounds(168, 208, 126, 31);
		panel.add(month_display);
		month_display.setVisible(false);
		JLabel salary_1;
		salary_display = new JLabel("");
		salary_display.setFont(new Font("Times New Roman", Font.PLAIN, 20));
		salary_display.setForeground(Color.WHITE);
		salary_display.setBounds(168, 239, 126, 31);
		panel.add(salary_display);
		salary_display.setVisible(false);
		
		JLabel newLabel = new JLabel("The Payroll For This Month For You");
		newLabel.setFont(new Font("Times New Roman", Font.PLAIN, 20));
		newLabel.setForeground(Color.WHITE);
		newLabel .setBounds(10, 140, 316, 46);
		panel.add(newLabel);
		newLabel.setVisible(false);
		
		JLabel newLabel_2 = new JLabel("Your taxes does not inculude to this\n check.");
		newLabel_2.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		newLabel_2.setForeground(Color.WHITE);
		newLabel_2 .setBounds(10, 312, 316, 31);
		panel.add(newLabel_2);
		newLabel_2.setVisible(false);
		JLabel newLabel_3 = new JLabel("If you want to get more details about your payroll please");
		newLabel_3.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		newLabel_3.setForeground(Color.WHITE);
		newLabel_3 .setBounds(10, 334, 326, 31);
		panel.add(newLabel_3);
		newLabel_3.setVisible(false);
		JLabel newLabel_4 = new JLabel("please prepare an objction to your boss!");
		newLabel_4.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		newLabel_4.setForeground(Color.WHITE);
		newLabel_4 .setBounds(10, 354, 316, 31);
		panel.add(newLabel_4);
		newLabel_4.setVisible(false);
		
		Button button_1 = new Button("Search");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lblNewLabel.setVisible(true);
				salary_message.setVisible(true);
				month_display.setVisible(true);
				salary_display.setVisible(true);
				newLabel.setVisible(true);
				newLabel_2.setVisible(true);
				newLabel_3.setVisible(true);
				newLabel_4.setVisible(true);
				Function f = new Function();
				ResultSet rs = null;
				String fn = "fname";
				
				rs = f.find(textField.getText());
				try {
					if (rs.next()) {
						
						salary_display.setText(rs.getString("salary"));
						month_display.setText(rs.getString("month"));
						
					} else {
						JOptionPane.showMessageDialog(null, "NO DATA FOR THIS ID");
					}
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(null, ex.getMessage());
				}
				
			}
		});
		button_1.setForeground(Color.WHITE);
		button_1.setBackground(Color.RED);
		button_1.setBounds(147, 90, 63, 20);
		panel.add(button_1);
		
		
		
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(345, -70, 483, 611);
		contentPane.add(panel_1);
		
		JLabel label = new JLabel("Payroll S");
		panel_1.add(label);
		label.setForeground(Color.WHITE);
		label.setFont(new Font("Tahoma", Font.PLAIN, 20));
		label.setVerticalAlignment(SwingConstants.TOP);
		label.setIcon(new ImageIcon("D:\\Developer\\Developing Workspace\\Eclipse\\nesenesel\\src\\back.gif"));
		JLabel lblEmployeePayrollSystem = new JLabel("Employee Payroll System");
		lblEmployeePayrollSystem.setForeground(Color.WHITE);
		lblEmployeePayrollSystem.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblEmployeePayrollSystem.setBounds(10, 11, 281, 38);
	}
}
