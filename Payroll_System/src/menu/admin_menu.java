package menu;
import login.login;
import utilities_admin.add_delete.add_delete_page;
import utilities_admin.see_objections;
import utilities_admin.transaction;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import login.login;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;



public class admin_menu extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					admin_menu frame = new admin_menu();
					frame.setUndecorated(true);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public admin_menu() {
		setBackground(Color.WHITE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 729, 476);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		JLabel label_1 = new JLabel("X");
		label_1.setBounds(686, 5, 37, 27);
		contentPane.add(label_1);
		label_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				System.exit(0);
			}
		});
		label_1.setHorizontalAlignment(SwingConstants.CENTER);
		label_1.setForeground(new Color(241, 57, 83));
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 18));
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(5, 13, 31));
		panel.setBounds(0, 0, 346, 490);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblAssistantPanel = new JLabel("Admin Page\r\n");
		lblAssistantPanel.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblAssistantPanel.setForeground(Color.RED);
		lblAssistantPanel.setBounds(10, 50, 188, 31);
		panel.add(lblAssistantPanel);
		
		JLabel label_2 = new JLabel("Employee Payroll System");
		label_2.setForeground(Color.WHITE);
		label_2.setFont(new Font("Tahoma", Font.PLAIN, 20));
		label_2.setBounds(10, 11, 336, 46);
		panel.add(label_2);
		
		Button button_2 = new Button("Transaction");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				transaction trans = new transaction();
				trans.main(null);
				dispose();
			}
		});
		button_2.setForeground(Color.WHITE);
		button_2.setBackground(Color.RED);
		button_2.setBounds(51, 233, 236, 36);
		panel.add(button_2);
		
		Button button_3 = new Button("Objections");
		button_3.addActionListener(new ActionListener() {//open objection page
			public void actionPerformed(ActionEvent arg0) {
				see_objections objection = new see_objections();
				objection.main(null);
				dispose();
			}
		});
		button_3.setForeground(Color.WHITE);
		button_3.setBackground(Color.RED);
		button_3.setBounds(51, 291, 236, 36);
		panel.add(button_3);
		
		Button button = new Button("Employees");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				add_delete_page adDelete = new add_delete_page();
				adDelete.main(null);
				dispose();
			}
		});
		button.setForeground(Color.WHITE);
		button.setBackground(Color.RED);
		button.setBounds(51, 179, 236, 36);
		panel.add(button);
		
		Button button_4 = new Button("Log out");
		button_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				login logout = new login();
				logout.main(null);
				dispose();
			}
		});
		button_4.setForeground(Color.WHITE);
		button_4.setBackground(Color.RED);
		button_4.setBounds(27, 405, 91, 22);
		panel.add(button_4);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(345, -70, 483, 611);
		contentPane.add(panel_1);
		
		JLabel label = new JLabel("Payroll S");
		panel_1.add(label);
		label.setForeground(Color.WHITE);
		label.setFont(new Font("Tahoma", Font.PLAIN, 20));
		label.setVerticalAlignment(SwingConstants.TOP);
		label.setIcon(new ImageIcon("D:\\Developer\\Developing Workspace\\Eclipse\\nesenesel\\src\\back.gif"));
		JLabel lblEmployeePayrollSystem = new JLabel("Employee Payroll System");
		lblEmployeePayrollSystem.setForeground(Color.WHITE);
		lblEmployeePayrollSystem.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblEmployeePayrollSystem.setBounds(10, 11, 281, 38);
		
	}
}
